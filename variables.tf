variable "credentials" {
  description = "User name and password"
  type = object({
    username = string
    password = string
  })
  sensitive = true
}

variable "base_values" {
  description = "region and project id"
  type = object({
    region = number
    project = number
  })
  default = {
    project = 154151
    region = 10
  }
}

variable "network_values" {
  description = "CIDR block to use for network. i.e 192.168.1.0/24"
  type = object({
    cidr_block = string
  })
  default = {
    cidr_block = "192.168.1.0/24"
  }
}

variable "instance_values" {
  description = "instance count, flavour, OS version, volume size and SSH keypair"
  type = object({
    instance_count = number
    flavour = string
    os_version = string
    size = number
    keypair = string
  })
  default = {
    instance_count = 2
    flavour = "g1-standard-2-4"
    os_version = "ubuntu-20.04"
    size = 5
    keypair = "vova-key"
  }
}

