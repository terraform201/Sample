output "network_out" {
  value = gcore_network.base-network
}

output "subnet_out" {
  value = gcore_subnet.base-subnet
}