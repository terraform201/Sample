terraform {
  required_version = ">= 1.1.3, <= 1.2.0"
  required_providers {
    gcore = {
      source  = "G-Core/gcorelabs"
      version = ">= 0.3.2"
    }
  }
}

resource "gcore_network" "base-network" {
  name = "${var.env_prefix}-network"
  region_id = var.region
  project_id = var.project
}

resource "gcore_subnet" "base-subnet" {
  name = "${var.env_prefix}-subnet"
  region_id = var.region
  project_id = var.project
  cidr = var.cidr_block
  network_id = gcore_network.base-network.id
  enable_dhcp = true
}