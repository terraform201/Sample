terraform {
  required_version = ">= 1.1.3, <= 1.2.0"
  required_providers {
    gcore = {
      source  = "G-Core/gcorelabs"
      version = ">= 0.3.2"
    }
  }
}

resource "gcore_securitygroup" "base-securitygroup"{
  name = "${var.env_prefix}-securitygroup"
  region_id = var.region
  project_id = var.project

  security_group_rules {
    direction = "egress"
    ethertype = "IPv4"
    protocol = "icmp"
  }
    security_group_rules {
    direction  = "egress"
    ethertype  = "IPv4"
    port_range_max = 80
    port_range_min = 80
    protocol   = "tcp"
  }
  security_group_rules {
    direction  = "ingress"
    ethertype  = "IPv4"
    port_range_max = 80
    port_range_min = 80
    protocol   = "tcp"
  }
  security_group_rules {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "icmp"
  }
  security_group_rules {
    direction = "egress"
    protocol = "vrrp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "udplite"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "udp"
    ethertype = "IPv4"
    port_range_min = 26
    port_range_max = 65535
  }
  security_group_rules {
    direction = "egress"
    protocol = "udp"
    port_range_min = 1
    port_range_max = 24
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "ingress"
    protocol = "udp"
    port_range_min = 3389
    port_range_max = 3389
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "tcp"
    port_range_min = 1
    port_range_max = 24
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "ingress"
    protocol = "tcp"
    port_range_min = 22
    port_range_max = 22
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "ingress"
    protocol = "tcp"
    port_range_min = 3389
    port_range_max = 3389
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "tcp"
    port_range_min = 26
    port_range_max = 65535
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "sctp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "rsvp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "pgm"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "ospf"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "igmp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "gre"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "esp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "egp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "dccp"
    ethertype = "IPv4"
  }
  security_group_rules {
    direction = "egress"
    protocol = "ah"
    ethertype = "IPv4"
  }
}