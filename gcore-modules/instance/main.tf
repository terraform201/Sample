terraform {
  required_version = ">= 1.1.3, <= 1.2.0"
  required_providers {
    gcore = {
      source  = "G-Core/gcorelabs"
      version = ">= 0.3.2"
    }
  }
}

data "gcore_image" "os_image" {
  name = var.os_version
  region_id = var.region
  project_id = var.project
}

resource "gcore_volume" "base-volume" {
  name = "${var.name}-volume"
  region_id = var.region
  project_id = var.project
  type_name = "ssd_hiiops"
  size = var.size
  image_id = data.gcore_image.os_image.id
}

resource "gcore_instance" "base-instance" {
  flavor_id = var.flavour
  region_id = var.region
  project_id = var.project
  keypair_name = var.keypair
  name = var.name
  userdata = filebase64("${path.root}/user-data/user-data.txt")
  security_group {
    id = var.securitygroup_id
    name = var.securitygroup_name
  }
  volume {
    source = "existing-volume"
    volume_id = gcore_volume.base-volume.id
    boot_index = 0
  }
  interface {
    type = "external"
    order = 0
  }
  interface {
    type = "subnet"
    network_id = var.network_id
    subnet_id = var.subnet_id
    order = 1
  }
}
