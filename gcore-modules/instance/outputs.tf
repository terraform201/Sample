data "gcore_instance" "existing-instance" {
  region_id = var.region
  project_id = var.project
  name = gcore_instance.base-instance.name
}

output "instance_ip" {
  value = element(data.gcore_instance.existing-instance.interface, 1).ip_address
}
