terraform {
  required_version = ">= 1.1.3, <= 1.2.0"
  required_providers {
    gcore = {
      source  = "G-Core/gcorelabs"
      version = ">= 0.3.2"
    }
  }
}

locals {
  lb_flavor = "lb1-1-2"
}

resource "gcore_loadbalancer" "base-balancer" {
  region_id = var.region
  project_id = var.project
  name = "${var.env_prefix}-balancer"
  flavor = local.lb_flavor
  listener {
    name = "${var.env_prefix}-listener"
    protocol = "HTTP"
    protocol_port = 80
  }
}

resource "gcore_lbpool" "base-listener-pool" {
  region_id = var.region
  project_id = var.project
  name = "${var.env_prefix}-listener-pool"
  protocol = "HTTP"
  lb_algorithm = "ROUND_ROBIN"
  loadbalancer_id = gcore_loadbalancer.base-balancer.id
  listener_id = gcore_loadbalancer.base-balancer.listener.0.id
  health_monitor {
    type = "PING"
    delay = 30
    max_retries = 1
    max_retries_down = 10
    timeout = 10
  }
}

resource "gcore_lbmember" "base-balancer-member" {
  count = var.instance_count
  region_id = var.region
  project_id = var.project
  pool_id = gcore_lbpool.base-listener-pool.id
  address = "${element(var.instance_ip, count.index)}"
  protocol_port = 80
  subnet_id = var.subnet_id
  weight = 1
}