# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/g-core/gcorelabs" {
  version     = "0.3.2"
  constraints = ">= 0.3.2, ~> 0.3.2"
  hashes = [
    "h1:j4VGLm1l77Rtf7oZ6mXiaEcjqTiNwuDpdXnQzZe0ug0=",
    "zh:17d575780d28e53f914d3b516870538a1a43579b175f426e9343fea9dcf8519e",
    "zh:18321cc75071464abef0fe54c9bcd89d88b63b81c7acb2a32792c2c14097a8a5",
    "zh:2a70389dfb6b73af38a5987f6a378a2781371d84b612c1a8c972393a8b76ed8e",
    "zh:47b8c05d04d376030660186b48e806b0589a478db6815b21db55b2b6d1a760a8",
    "zh:53f3fe1ede415004e68b780cc9267b06702107e8d870e71c9c10d903d9bc8c8b",
    "zh:8dc6cf5fbe66cc957d9a4f3d4444729fdc19defcac851e7df525d86fcce4139b",
    "zh:9ab9286679d791f627cae3d3fa32c825ce95fca81a7145a9884760bbb530707a",
    "zh:9c65d23c082ae4fadffe16a6f4ea44d5902a435248f77c0790e1849e9b359291",
    "zh:ca44cc48ea09fa415428fe6418c44dd0bb90a4de1fc10490831d342d5a892268",
    "zh:d8a96b7b6ccddd35f24821870ecec9c0c51d72cba2a6f981cf801caf37800e47",
    "zh:ef72487b926f0d4a7b43ca6607fba0c19b54a1f53953cbba09c8a7c25a7dadfb",
    "zh:f260d7b89fb359b2d5b28922114c7bf1b951022033715e7a2603a424821b846b",
    "zh:f89114c79b99115e08abb1335be78b168239a15ba2f24cfea1cab7eb829b327b",
  ]
}
