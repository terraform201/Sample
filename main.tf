terraform {
  required_version = ">= 1.1.3, <= 1.2.0"
  required_providers {
    gcore = {
      source  = "G-Core/gcorelabs"
      version = "~> 0.3.2"
    }
  }
}

locals {
  # Setting the environment prefix to current directory name
  env_prefix = basename(abspath(path.root))

  # Generating unique instance names
  instance_names = [
    for i in range(1, var.instance_values.instance_count+1) : { 
      uniq_name = "${local.env_prefix}-instance-${i}" 
    }
  ]
}

provider gcore {
  ignore_creds_auth_error = true // Should not be needed here. Reported in GCLOUD2-5489
  user_name = var.credentials.username
  password = var.credentials.password
  gcore_platform = "https://api.gcdn.co"
  gcore_api = "https://api.cloud.gcorelabs.com"
}

module "securitygroup" {
  source = "./gcore-modules/securitygroup"
  region = var.base_values.region
  project = var.base_values.project
  env_prefix = local.env_prefix
}


module "subnet" {
  source = "./gcore-modules/subnet"
  region = var.base_values.region
  project = var.base_values.project
  env_prefix = local.env_prefix
  cidr_block = var.network_values.cidr_block
}

module "instance" {
  source = "./gcore-modules/instance"
  for_each = {
    for name in local.instance_names : name.uniq_name => name
  }
  name = each.value.uniq_name
  region = var.base_values.region
  project = var.base_values.project
  env_prefix = local.env_prefix
  flavour = var.instance_values.flavour
  os_version = var.instance_values.os_version
  size = var.instance_values.size
  keypair = var.instance_values.keypair
  securitygroup_id = module.securitygroup.securitygroup_out.id
  securitygroup_name = module.securitygroup.securitygroup_out.name
  network_id = module.subnet.network_out.id
  subnet_id = module.subnet.subnet_out.id
}

module "balancer" {
  source = "./gcore-modules/balancer"
  region = var.base_values.region
  project = var.base_values.project
  env_prefix = local.env_prefix
  instance_count = var.instance_values.instance_count
  instance_ip = values(module.instance)[*].instance_ip
  subnet_id = module.subnet.subnet_out.id 
}