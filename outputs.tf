output "instance_ip_to_balancer" {
  value = values(module.instance)[*].instance_ip
}

output "balancer_ip" {
  value = module.balancer.balancer_ip
}